<?php
abstract  class Connector
{
	abstract  function logRequest();
	abstract  function logError();
	abstract  function loadData();
	abstract  function showResult($data);
}